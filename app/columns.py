class Column():
    '''Class containing all information regarding a single table column

    These information can be read into dictionaries or lists using build_dict
    and build_list. Some important combinations:

    Table AJAX: data, searchable, className, width
    Entry columns: data, icon, title
    '''

    def __init__(self, data, icon, title=None, title_short=None,
                 searchable=None, cls=None, width=None, datatype=None):
        self.data = data
        self.icon = icon
        self.title = title if title else self.data.capitalize()
        self.title_short = title_short if title_short else self.title
        self.searchable = searchable
        self.className = cls
        self.width = width
        self.datatype = datatype


column_definitions = {
    'DataSample': [
        Column('id', 'person_outline', 'ID'),
        Column('name', 'face', cls='mdl-data-table__cell--non-numeric', width='99%'),
        Column('energy', 'battery_charging_full', searchable=False),
        # user
        Column('updater', 'supervisor_account', cls='mdl-data-table__cell--non-numeric'),
        Column('comments', 'comment', cls='mdl-data-table__cell--non-numeric'),
    ],
    'MCSample': [
        Column('id', 'person_outline', 'ID'),
        Column('name', 'face', cls='mdl-data-table__cell--non-numeric', width='99%'),
        Column('energy', 'battery_charging_full', searchable=False),
        # generator
        Column('generator', 'computer', cls='mdl-data-table__cell--non-numeric'),
        # cross section
        Column('crosssection', 'pie_chart', 'Cross section (pb)', 'Xs (pb)', False),
        Column('crosssection_order', 'line_weight', 'Cross section order', 'Order', False, cls='mdl-data-table__cell--non-numeric'),
        Column('crosssection_ref', 'bookmark', 'Cross section reference', 'Ref.', cls='mdl-data-table__cell--non-numeric'),
        # filter efficiency
        Column('filterefficiency', 'filter_list', 'Filter efficiency', 'Filter eff.', False),
        Column('filterefficiency_ref', 'bookmark', 'Filter efficiency reference', 'Ref.', cls='mdl-data-table__cell--non-numeric'),
        # k factor
        Column('kfactor', 'show_chart', 'k-Factor', searchable=False),
        Column('kfactor_order', 'line_weight', 'k-Factor order', 'Order', False, cls='mdl-data-table__cell--non-numeric'),
        Column('kfactor_ref', 'bookmark', 'k-Factor reference', 'Ref.', cls='mdl-data-table__cell--non-numeric'),
        # user
        Column('updater', 'supervisor_account', cls='mdl-data-table__cell--non-numeric'),
        Column('comments', 'comment', cls='mdl-data-table__cell--non-numeric'),
    ],
    'DataSkim': [
        Column('id', 'person_outline', 'ID'),
        Column('sample_id', 'supervisor_account', 'Sample ID', 'Sample'),
        # Dataset
        Column('datasetpath', 'place', cls='mdl-data-table__cell--non-numeric mdl-data-table__cell--long'),
        Column('nevents', 'format_list_numbered', 'Number of events'),
        Column('size', 'folder', datatype='filesize'),
        Column('sites', 'business', cls='mdl-data-table__cell--non-numeric', datatype='list'),
        # Skimmer
        Column('version', 'rate_review', cls='mdl-data-table__cell--non-numeric'),
        Column('cmssw', 'build', 'CMSSW', cls='mdl-data-table__cell--non-numeric'),
        Column('globaltag', 'book', cls='mdl-data-table__cell--non-numeric'),
        # Run ranges
        Column('run_json', 'text_format', 'Run JSON', cls='mdl-data-table__cell--non-numeric'),
        Column('run_first', 'first_page', 'First Run'),
        Column('run_last', 'last_page', 'Last Run'),
        # Luminosity
        Column('processed_json', 'text_format', 'Processed JSON', cls='mdl-data-table__cell--non-numeric'),
        # Time and status
        Column('created', 'add_alarm', cls='mdl-data-table__cell--non-numeric'),
        Column('finished', 'access_alarm', cls='mdl-data-table__cell--non-numeric'),
        Column('deprecated', 'timer_off', cls='mdl-data-table__cell--non-numeric'),
        Column('private', 'snooze', cls='mdl-data-table__cell--non-numeric', datatype='list'),
        # User
        Column('owner', 'supervisor_account', cls='mdl-data-table__cell--non-numeric'),
        Column('updater', 'edit', cls='mdl-data-table__cell--non-numeric'),
        Column('comments', 'comment', cls='mdl-data-table__cell--non-numeric'),
    ],
    'DataFile': [
        Column('id', 'person_outline', 'ID'),
        Column('skim_id', 'supervisor_account', 'Skim ID'),
        # File
        Column('path', 'supervisor_account'),
        Column('nevents', 'format_list_numbered', 'Number of events'),
        Column('size', 'folder', datatype='filesize')
    ],
    'MCSkim': [
        Column('id', 'person_outline', 'ID'),
        Column('sample_id', 'supervisor_account', 'Sample ID', 'Sample'),
        # Dataset
        Column('datasetpath', 'place', cls='mdl-data-table__cell--non-numeric mdl-data-table__cell--long'),
        Column('nevents', 'format_list_numbered', 'Number of events', 'Num Events'),
        Column('size', 'folder', datatype='filesize'),
        Column('sites', 'business', cls='mdl-data-table__cell--non-numeric', datatype='list'),
        # Skimmer
        Column('version', 'rate_review', cls='mdl-data-table__cell--non-numeric'),
        Column('cmssw', 'build', 'CMSSW', cls='mdl-data-table__cell--non-numeric'),
        Column('globaltag', 'book', cls='mdl-data-table__cell--non-numeric'),
        # Time and status
        Column('created', 'add_alarm', cls='mdl-data-table__cell--non-numeric'),
        Column('finished', 'access_alarm', cls='mdl-data-table__cell--non-numeric'),
        Column('deprecated', 'timer_off', cls='mdl-data-table__cell--non-numeric'),
        Column('private', 'snooze', cls='mdl-data-table__cell--non-numeric', datatype='list'),
        # User
        Column('owner', 'supervisor_account', cls='mdl-data-table__cell--non-numeric'),
        Column('updater', 'edit', cls='mdl-data-table__cell--non-numeric'),
        Column('comments', 'comment', cls='mdl-data-table__cell--non-numeric'),
    ],
    'MCFile': [
        Column('id', 'person_outline', 'ID'),
        Column('skim_id', 'supervisor_account', 'Skim ID'),
        # File
        Column('path', 'supervisor_account'),
        Column('nevents', 'format_list_numbered', 'Number of events'),
        Column('size', 'folder', datatype='filesize')
    ],
}


def build_dict(classname, attributes=[], exclude=[], only=[]):
    '''Builds a dictionary out of the columns

    Selecting columns is done through the "attributes", which take the data
    (e.g. "owner") of a column. All specified attributes are put into a
    dictionary. Both "exclude" and "only" take lists of attribute values with
    which to filter the result.
    '''

    columns = column_definitions[classname]
    result = []

    for column in columns:
        # Skip columns not in 'only'
        if only and column.data not in only:
            continue
        # Skip columns that are in 'exclude'
        if exclude and column.data in exclude:
            continue
        # Copy columns to row in results
        element = {}
        for attribute in attributes:
            # Only insert value if it isn't None
            value = getattr(column, attribute)
            if value is not None:
                element[attribute] = value
        result.append(element)

    return result


def build_list(classname, attribute, exclude=[], only=[]):
    '''Builds a list out of the columns

    Selecting columns is done through the "attributes", which take the data
    (e.g. "owner") of a column. All specified attributes are put into a list.
    Both "exclude" and "only" take lists of attribute values with which to
    filter the result. '''

    columns = column_definitions[classname]
    result = []

    for column in columns:
        # Skip columns not in 'only'
        if only and column.data not in only:
            continue
        # Skip columns that are in 'exclude'
        if exclude and column.data in exclude:
            continue
        # Copy column to row in results
        result.append(getattr(column, attribute))

    return result
