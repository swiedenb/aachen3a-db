'''Definition of the database's tables

This file defines all the SQL tables that are contained within the database.

While it might seem like there is a lot of code duplication, versioning the
individual tables with sqlalchemy-continuum prevents a "joined inheritance"
approach. There, one would have, for example, a base table Sample which is
joined with DataSample to add the additional columns for data entries. However,
this would mean versioning both the base and the specific table individually.
Subsequently one would have to manually manage the changes to have an overview
of both tables. That makes this option not viable.
'''

# sqlalchemy imports
from sqlalchemy import inspect, func, Column, ForeignKey
from sqlalchemy import Integer, BigInteger, Float, String, DateTime
from sqlalchemy.orm import relationship, configure_mappers
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects import postgresql
from sqlalchemy.util import symbol
# sqlalchemy-utils imports
from sqlalchemy_utils import aggregated
# sqla versioning
import sqlalchemy_continuum

# Initiate (native PostgreSQL) versioning of objects
# Note that there is no User class at the moment
sqlalchemy_continuum.make_versioned(user_cls=None)
                                    # options={'native_versioning': True})
# Define sqlalchemy Base object
Base = declarative_base()


class DataSample(Base):
    '''Sample table for data entries'''

    __tablename__ = 'datasample'
    __versioned__ = {}

    # Unique identifier
    id = Column(Integer, primary_key=True)
    # Sample
    name = Column(String(150), unique=True)
    energy = Column(Integer)
    # User
    updater = Column(String(20))
    comments = Column(String(300))

    # Relation to DataSkim objects
    skims = relationship('DataSkim', back_populates='sample',
                         cascade='all, delete-orphan',
                         order_by='asc(DataSkim.id)')
    def __repr__(self):
        return '<DataSample {0}>'.format(self.id)


class DataSkim(Base):
    '''Skim table for data entries'''

    __tablename__ = 'dataskim'
    __versioned__ = {
        'exclude': ['nevents', 'size']
    }

    # Unique identifier and parent sample
    id = Column(Integer, primary_key=True)
    sample = relationship('DataSample', back_populates='skims')
    sample_id = Column(Integer, ForeignKey('datasample.id'))
    # User
    owner = Column(String(20))
    updater = Column(String(20))
    comments = Column(String(300))
    # Sample
    datasetpath = Column(String(300))
    sites = Column(postgresql.ARRAY(String(40), dimensions=1), default=[])
    files = relationship('DataFile', cascade='all, delete-orphan',
                         lazy='dynamic')
    # Aggregated columns based on the file relationships
    @aggregated('files', Column(BigInteger, default=0))
    def nevents(self):
        return func.coalesce(func.sum(DataFile.nevents), 0)
    @aggregated('files', Column(BigInteger, default=0))
    def size(self):
        return func.coalesce(func.sum(DataFile.size), 0)
    # Skimmer
    version = Column(String(40))
    cmssw = Column(String(40))
    globaltag = Column(String(80))
    # Time and status
    created = Column(DateTime(True))
    finished = Column(DateTime(True))
    deprecated = Column(DateTime(True))
    private = Column(postgresql.ARRAY(String(20), dimensions=1), default=[])
    # Run ranges
    run_json = Column(String(50))
    run_first = Column(Integer)
    run_last = Column(Integer)
    # Luminosity
    processed_json = Column(String(700))

    def __repr__(self):
        return '<DataSkim {0}>'.format(self.id)


class DataFile(Base):
    '''Table for data skim files'''

    __tablename__ = 'datafile'

    # IDs
    id = Column(BigInteger, primary_key=True)
    skim_id = Column(Integer, ForeignKey('dataskim.id'))
    # File
    path = Column(String(700))
    nevents = Column(Integer, default=0)
    size = Column(BigInteger, default=0)

    def __repr__(self):
        return '<File {0}, size={1}>'.format(self.id, self.size)


class MCSample(Base):
    '''Sample table for simulation entries'''

    __tablename__ = 'mcsample'
    __versioned__ = {}

    # Unique identifier
    id = Column(Integer, primary_key=True)
    # sample
    name = Column(String(150), unique=True)
    energy = Column(Integer)
    # User
    updater = Column(String(20))
    comments = Column(String(300))
    # Relation to MCSkim objects
    skims = relationship('MCSkim', back_populates='sample',
                         cascade='all, delete-orphan',
                         order_by='asc(MCSkim.id)')
    # Generator
    generator = Column(String(30))
    filterefficiency = Column(Float)
    filterefficiency_ref = Column(String(300))
    # Cross section
    crosssection = Column(Float)
    crosssection_order = Column(String(10))
    crosssection_ref = Column(String(300))
    kfactor = Column(Float)
    kfactor_order = Column(String(10))
    kfactor_ref = Column(String(300))

    def __repr__(self):
        return '<MCSample {0}>'.format(self.id)


class MCSkim(Base):
    '''Skim table for Monte Carlo entries'''

    __tablename__ = 'mcskim'
    __versioned__ = {
        'exclude': ['nevents', 'size']
    }

    # Unique identifier and parent sample
    id = Column(Integer, primary_key=True)
    sample = relationship('MCSample', back_populates='skims')
    sample_id = Column(Integer, ForeignKey('mcsample.id'))
    # User
    owner = Column(String(20))
    updater = Column(String(20))
    comments = Column(String(300))
    # Sample
    datasetpath = Column(String(300))
    sites = Column(postgresql.ARRAY(String(40), dimensions=1), default=[])
    files = relationship('MCFile', cascade='all, delete-orphan', lazy='dynamic')
    # Aggregated columns based on the file relationships
    @aggregated('files', Column(BigInteger, default=0))
    def nevents(self):
        return func.coalesce(func.sum(MCFile.nevents), 0)
    @aggregated('files', Column(BigInteger, default=0))
    def size(self):
        return func.coalesce(func.sum(MCFile.size), 0)
    # Skimmer
    version = Column(String(40))
    cmssw = Column(String(40))
    globaltag = Column(String(80))
    # Time and status
    created = Column(DateTime(True))
    finished = Column(DateTime(True))
    deprecated = Column(DateTime(True))
    private = Column(postgresql.ARRAY(String(20), dimensions=1), default=[])

    def __repr__(self):
        return '<MCSkim {0}>'.format(self.id)


class MCFile(Base):
    '''Table for Monte Carlo skim files'''

    __tablename__ = 'mcfile'

    # IDs
    id = Column(BigInteger, primary_key=True)
    skim_id = Column(Integer, ForeignKey('mcskim.id'))
    # File
    path = Column(String(700))
    nevents = Column(Integer, default=0)
    size = Column(BigInteger, default=0)

    def __repr__(self):
        return '<File {0}, size={1}>'.format(self.id, self.size)


# Configure mappers for versioning after having defined all the models
configure_mappers()


def summarize_columns(model, exclude=None):
    '''Create summary {name: type, ...} for all columns of model

    One can specify column names to skip via the `exclude` set. While
    relationships are not strictly columns, they are also interpreted as such.
    Here, Many-to-One relationships yield list and One-to-Many yield the
    remote\'s primary key python type.
    '''

    # Ensure exclude to be a empty set by default
    exclude = exclude if exclude is not None else set()

    # Attempt to inspect the model
    try:
        model = inspect(model)
    except NoInspectionAvailable:
        return {}

    summary = {}
    # Extract python type from each column
    for column in model.columns:
        # Skip specified columns
        if column.name in exclude:
            continue
        # Store python type of column
        try:
            summary[column.name] = column.type.python_type.__name__
        except NotImplementedError:
            continue

    # Infer python type for each relationship
    if model.relationships:
        for name, relationship in dict(model.relationships).items():
            # Skip specified columns
            if name in exclude:
                continue
            # One-to-many are lists, e.g. skims = [skim1, skim2, ...]
            if relationship.direction is symbol('ONETOMANY'):
                summary[name] = list.__name__
            # Many-to-one depend on type of primary key on remote side
            elif relationship.direction is symbol('MANYTOONE'):
                summary[name] = next(iter(relationship.remote_side))\
                                .type.python_type.__name__
    # Return summary for model columns and relationships {name: type, ...}
    return summary
