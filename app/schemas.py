'''
'''
from marshmallow_sqlalchemy import ModelSchema, field_for
from marshmallow import fields
from . import session, models

# Base for other schemas to inhert from
class BaseSchema(ModelSchema):
    # Don't allow for id changes
    id = fields.Int(dump_only=True)
    class Meta:
        sqla_session = session
        # Exclude versioning and sample relationships
        exclude = ('versions', 'sample')

# Schemas for model (de)serialization
class DataFileSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = models.DataFile


class DataSkimSchema(BaseSchema):
    # Nest one-to-many file relationships
    files = fields.Nested(DataFileSchema, many=True)
    # Aggregated attributes, not to be modified manually
    nevents = fields.Int(dump_only=True)
    size = fields.Int(dump_only=True)
    class Meta(BaseSchema.Meta):
        model = models.DataSkim


class DataSampleSchema(BaseSchema):
    # Don't allow relationship changes
    skims = field_for(models.DataSample, 'skims', dump_only=True)
    class Meta(BaseSchema.Meta):
        model = models.DataSample


class NestedDataSampleSchema(BaseSchema):
    # Nest one-to-many file relationships
    skims = fields.Nested(DataSkimSchema, many=True, dump_only=True)
    class Meta(BaseSchema.Meta):
        model = models.DataSample


class MCFileSchema(BaseSchema):
    class Meta(BaseSchema.Meta):
        model = models.MCFile


class MCSkimSchema(BaseSchema):
    # Nest one-to-many file relationships
    files = fields.Nested(MCFileSchema, many=True)
    # Aggregated attributes, not to be modified manually
    nevents = fields.Int(dump_only=True)
    size = fields.Int(dump_only=True)
    class Meta(BaseSchema.Meta):
        model = models.MCSkim


class MCSampleSchema(BaseSchema):
    # Don't allow relationship changes
    skims = field_for(models.MCSample, 'skims', dump_only=True)
    class Meta(BaseSchema.Meta):
        model = models.MCSample


class NestedMCSampleSchema(BaseSchema):
    # Nest one-to-many file relationships
    skims = fields.Nested(MCSkimSchema, many=True, dump_only=True)
    class Meta(BaseSchema.Meta):
        model = models.MCSample

# Initialize schemas
schema_NestedDataSample = NestedDataSampleSchema()
schema_DataSample = DataSampleSchema()
schema_DataSkim = DataSkimSchema()
schema_DataFile = DataFileSchema()
schema_NestedMCSample = NestedMCSampleSchema()
schema_MCSample = MCSampleSchema()
schema_MCSkim = MCSkimSchema()
schema_MCFile = MCFileSchema()
