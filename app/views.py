# flask imports
from flask import request, send_from_directory, render_template
from flask import redirect, url_for, abort, flash
# sqlalchemy imports
from sqlalchemy.sql import func

# local imports
from . import app, session, models, forms
from .columns import build_list, build_dict

# Supportive functions
def get_classname(name):
    '''Return name of class based on lowercase names

    Effectively maps lowercase class names to actual model classes via a
    dictionary.
    '''

    classes = {
        # Data
        'datasample': 'DataSample',
        'datasamples': 'DataSample',
        'dataskim': 'DataSkim',
        'dataskims': 'DataSkim',
        'datafile': 'DataFile',
        # Simulation
        'mcsample': 'MCSample',
        'mcsamples': 'MCSample',
        'mcskim': 'MCSkim',
        'mcskims': 'MCSkim',
        'mcfile': 'MCFile'
    }
    # Return name of class if match is found
    if name not in classes:
        print(name)
        abort(404)
    return classes[name]


# Static files
# @app.route('/sitemap.xml')
@app.route('/robots.txt')
def send_file():
    return send_from_directory(app.static_folder, request.path[1:])


# Home
@app.route('/')
@app.route('/index')
def index():
    def query_statistics(model, sqlfilter):
        # Number of entries
        num = session.query(model).filter(sqlfilter).count()
        # If there are no entries, return 0s
        if not num: return 0, 0
        # Sum of entry sizes
        size = session.query(func.sum(model.size)).filter(sqlfilter).scalar()
        # Return statistics
        return num, int(size)

    # Get username
    username = request.environ.get('HTTP_X_REMOTE_USER')
    # Get statistics for skims
    sizes = {'common': 0, 'private': 0, 'user': 0}
    numbers = {'common': 0, 'private': 0, 'user': 0}
    # Iterate over all skim models to add their individual contributions
    for model in [models.DataSkim, models.MCSkim]:
        # Sum up number and sizes of skims for each category
        for category, expr in zip(['common', 'private', 'user'],
                                  [model.private == [],  # Common skims
                                   model.private != [],  # Private skims
                                   model.private.any(username)]):  # Users skims
            num, size = query_statistics(model, expr)
            numbers[category] += num
            sizes[category] += size

    # Format for Javascript pie-charts
    fmt_size = app.jinja_env.filters['filesizeformat']

    all_num = {
        'id': 'pie-all-num',
        'title': 'Amount of skims',
        'data': [['Common', numbers['common']], ['Private', numbers['private']]]
    }
    all_size = {
        'id': 'pie-all-size',
        'title': 'Size of files',
        # Additional column for the formatted file-size tooltip
        'column': {'type': 'string', 'role': 'tooltip'},
        'data': [
            ['Common', sizes['common'], 'Common\n' + fmt_size(sizes['common'])],
            ['Private', sizes['private'], 'Private\n' + fmt_size(sizes['private'])]
        ]
    }
    user_num = {
        'id': 'pie-user-num',
        'title': 'Your fraction of private skims',
        'data': [['Others', numbers['private'] - numbers['user']],
                 ['Yours', numbers['user']]]
    }
    user_size = {
        'id': 'pie-user-size',
        'title': 'Your fraction of private files',
        # Additional column for the formatted file-size tooltip
        'column': {'type': 'string', 'role': 'tooltip'},
        'data': [
            ['Others', sizes['private'] - sizes['user'],
             'Others\n' + fmt_size(sizes['private'] - sizes['user'])],
            ['Yours ', sizes['user'],
             'Yours\n' + fmt_size(sizes['user'])]
        ]
    }

    return render_template('index.html', title='Home', username=username,
                           charts=[all_num, all_size, user_num, user_size])


@app.route('/<string:name>')
def table(name):
    # Name used for AJAX communication and column building
    classname = get_classname(name)
    # Titles of each column with custom link already defined in HTML
    column_titles = build_list(classname, 'title_short',
                               exclude=['processed_json'])
    column_titles.insert(0, 'Link')
    # Content and content attributes of each column
    column_content = build_dict(classname,
                                ['data', 'searchable', 'className', 'width'],
                                exclude=['processed_json'])

    return render_template('table.html',
                           title=classname,
                           column_titles=column_titles,
                           column_content=column_content,
                           classname=classname)


@app.route('/<string:name>/<int:id>')
def entry(name, id):
    # Samples
    if name.endswith('samples'):
        # Retrieve name of class to setup remaining objects
        sample_class = get_classname(name)
        skim_class = get_classname(name.replace('sample', 'skim'))
        # Define model columns which to show
        sample_columns = build_dict(sample_class, ['data', 'icon', 'title'])
        skim_columns = build_dict(skim_class, ['data', 'icon', 'title', 'datatype'],
                                  only=['id', 'datasetpath', 'version', 'nevents'])
        # Perform database query
        model = getattr(models, sample_class, None)
        sample = session.query(model).filter_by(id=id).one_or_none()
        # Define action button links
        skim_actions = [{'url': '/{}s/%d'.format(skim_class.lower()),
                         'text': 'view'}]
        sample_actions = [{'url': url_for('entry_edit', name=name, id=id),
                           'text': 'edit'}]

        # Render the template for samples
        return render_template('entry.html',
                               title='{0} - {1}'.format(sample.name, sample.id),
                               sample=sample,
                               sample_columns=sample_columns,
                               sample_actions=sample_actions,
                               skims=sample.skims,
                               skim_columns=skim_columns,
                               skim_actions=skim_actions,
                               history=sample.versions)
    # Skims
    elif name.endswith('skims'):
        # Retrieve names of class to setup remaining objects
        skim_class = get_classname(name)
        sample_class = get_classname(name.replace('skim', 'sample'))
        # Define model columns which to show
        skim_columns = build_dict(skim_class, ['data', 'icon', 'title', 'datatype'])
        sample_columns = build_dict(sample_class, ['data', 'icon', 'title'])
        # Perform database query
        model = getattr(models, skim_class)
        skim = session.query(model).filter_by(id = id).one_or_none()
        sample = skim.sample
        # Define action button links
        skim_actions = [{'url': '/{}s/%d/edit'.format(skim_class.lower()),
                         'text': 'edit'}]
        sample_actions = [{
            'url': url_for('entry', name=sample_class.lower() + 's', id=skim.sample_id),
            'text': 'view'
        }]

        # Render the template for skims
        return render_template('entry.html',
                               title='{0} - {1}'.format(skim.id, sample.name),
                               sample=sample,
                               sample_columns=sample_columns,
                               sample_actions=sample_actions,
                               skims=[skim],
                               skim_columns=skim_columns,
                               skim_actions=skim_actions,
                               history=skim.versions,
                               files=skim.files.all())


    # Abort if URL is poorly constructed
    abort(404)



@app.route('/<string:name>/<int:id>/edit', methods=['GET', 'POST'])
def entry_edit(name, id):
    # Retrieve proper name of class to setup remaining objects
    entryclass = get_classname(name)
    model = getattr(models, entryclass)
    entry = session.query(model).filter_by(id=id).one_or_none()

    # Define columns for which to allow editing
    columns = build_list(entryclass, 'data',
                         exclude=['id', 'sample_id', 'nevents', 'size'])
    values = {column: getattr(entry, column) for column in columns}
    values['updater'] = request.environ.get('HTTP_X_REMOTE_USER')

    # Create form based on columns
    form = getattr(forms, entryclass + 'Form')(request.form, **values)
    # If an edit is being requested, validate form and insert entry
    if request.method == 'POST' and form.validate():
        form.populate_obj(entry)
        session.commit()
        flash('Updated {0} {1}'.format(entryclass, entry.id))
        return redirect(url_for('entry', name=name, id=id))

    return render_template('entry_edit.html',
                           title='Edit {0}'.format(entry.id),
                           name=name,
                           entry=entry,
                           columns=columns,
                           form=form)
