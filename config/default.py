import os

# Forward exceptions, to be able to see them in logs
PROPAGATE_EXCEPTIONS = True

# Setup database paths
SQLALCHEMY_DATABASE_URI = '{engine}://{user}:{password}@dbod-ac3a-db.cern.ch:6605/{name}'.format(
    engine=os.environ.get('DATABASE_ENGINE'),
    user=os.environ.get('DATABASE_USER'),
    password=os.environ.get('DATABASE_PASSWORD'),
    name=os.environ.get('DATABASE_NAME')
)
# Turn off tracking to avoid major costs
SQLALCHEMY_TRACK_MODIFICATIONS = False
# Secret key (for flask session management etc.)
SECRET_KEY = os.environ.get('SECRET_KEY')
