# Set number of workers to fixed value
# This should be deduced automatically, but does not seem to work for Openshift
# docker instances
workers = 4
# Increase timeout for workers
# Depending on boot time of workers, they may otherwise time out before being
# ready
timeout = 120
# Increase length of HTTP request line (default=4094)
# Necessary for datatables AJAX communication with many columns, but do not
# want to remove limit (=0) to avoid accidental DDoS
limit_request_line = 8190
