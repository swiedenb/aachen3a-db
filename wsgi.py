'''WSGI exectuable for production

This script is the exectuable called by the Web Server Gateway Interface called
by gunicorn (green unicorn). By default, it expectes the application to be
called 'application', which is why the import renames the app.

Using app.run() directly without a WSGI is not recommended, as it does not meet
security standards and performance requirements.
'''

# import the application itself
from app import app as application
